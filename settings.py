import os

PROJECT_PATH = os.path.dirname(os.path.dirname(__file__))
ASTRO_PATH = PROJECT_PATH + '/network-analysis/input/ca-AstroPh/out.ca-AstroPh'
LIVEMOCHA_PATH = PROJECT_PATH + '/network-analysis/input/livemocha/out.livemocha'
# third dataset
