import networkx as nx
from networkx.drawing.tests.test_pylab import plt

from python.Analyzer import Analyzer
from python.distribution import degree_dist, clustering_dist, shortest_path_dist
from settings import ASTRO_PATH, LIVEMOCHA_PATH

paths = [ASTRO_PATH, LIVEMOCHA_PATH]
tags = ['astro', 'livemocha']

for i, path in enumerate(paths):
    # analysis
    analyzer = Analyzer(path)
    G = analyzer.load()
    results = analyzer.analyze()
    print(results)

    # visualization
    degree_dist(G, tags[i])
    clustering_dist(G, tags[i])
    shortest_path_dist(G, tags[i])

    nx.draw(G)
    plt.savefig(f"{path}_base.png")
    nx.draw(G, pos=nx.spring_layout(G))
    plt.savefig(f"{path}_FR.png")
    nx.draw(G, pos=nx.circular_layout(G))
    plt.savefig(f"{path}_circular.png")
    nx.draw(G, pos=nx.random_layout(G))
    plt.savefig(f"{path}_random.png")

# shortest paths, diameter, betweenness, closeness - long computation
