import networkx as nx
import numpy as np

from python.utils import timeit


class Analyzer:
    def __init__(self, path):
        self.path = path
        self.G = None

    @timeit
    def load(self):
        self.G = nx.read_adjlist(self.path)
        print(nx.info(self.G))
        return self.G

    @timeit
    def analyze(self):
        print('Analysis started')
        results = {'degree': self.degree(), 'diameter': self.diameter(), 'betweenness': self.betweenness(),
                   'closeness': self.closeness(), 'clustering_coefficient': self.clustering_coefficient(),
                   'pagerank': self.pagerank(), 'num_of_con_comp': self.num_of_con_comp(), 'density': self.density(),
                   'shortest_paths': self.shortest_paths()}
        return results

    @timeit
    def degree(self):
        degree = nx.degree(self.G)
        # print('Degree', degree)
        return degree

    @timeit
    def diameter(self):
        is_connected = nx.is_connected(self.G)
        if is_connected:
            diam = nx.diameter(self.G)
        else:
            sub_graph = nx.subgraph(self.G, list(nx.connected_components(self.G))[0])
            diam = nx.diameter(sub_graph)
        print('Diameter', diam)
        return diam

    @timeit
    def betweenness(self):
        betweenness = nx.betweenness_centrality(self.G)
        avg_betweenness = np.mean(list(betweenness.values()))
        print('Betweenness', avg_betweenness)
        return avg_betweenness

    @timeit
    def closeness(self):
        closeness = nx.closeness_centrality(self.G)
        avg_closeness = np.mean(list(closeness.values()))
        print('Closeness', avg_closeness)
        return avg_closeness

    @timeit
    def clustering_coefficient(self):
        coeff = nx.average_clustering(self.G)
        print('Clustering coefficient', coeff)
        return coeff

    @timeit
    def pagerank(self):
        pagerank = nx.pagerank(self.G)
        avg_pagerank = np.mean(list(pagerank.values()))
        print('Pagerank', avg_pagerank)
        return avg_pagerank

    @timeit
    def num_of_con_comp(self):
        num = nx.number_connected_components(self.G)
        print('Num. of con. comp', num)
        return num

    @timeit
    def density(self):
        density = nx.density(self.G)
        print('Density', density)
        return density

    @timeit
    def shortest_paths(self):
        is_connected = nx.is_connected(self.G)
        if is_connected:
            shortest_path = nx.average_shortest_path_length(self.G)
        else:
            connected_components = nx.connected_components(self.G)
            shortest_path = np.mean([
                nx.average_shortest_path_length(C)
                for C in (self.G.subgraph(c).copy() for c in connected_components)
                ])
        print('Shortest paths', shortest_path)
        return shortest_path
