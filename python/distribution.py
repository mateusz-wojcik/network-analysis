import collections
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


def degree_dist(G, name):
    degree_sequence = sorted([d for n, d in G.degree()], reverse=True)
    degreeCount = collections.Counter(degree_sequence)
    deg, cnt = zip(*degreeCount.items())
    fig, ax = plt.subplots(figsize=(20, 10))
    plt.bar(deg, cnt, width=0.80, color='b')
    plt.title("Degree distribution")
    plt.ylabel("Count")
    plt.xlabel("Degree")
    ax.set_xticks([d + 0.4 for d in deg])
    ax.set_xticklabels(deg)
    plt.savefig(f'{name}_degree_dist.png')


def clustering_dist(G, name):
    arr = []
    for v in nx.clustering(G).values():
        arr.append(v)
    heights, bins = np.histogram((arr), bins=50)
    fig, ax = plt.subplots(figsize=(20, 10))
    plt.title("Clustering coefficient distribution")
    plt.ylabel("Count")
    plt.xlabel("Clustering coefficient")
    plt.bar(bins[:-1], heights, width=(max(bins) -
                                       min(bins)) / len(bins), color='b', alpha=0.5)
    plt.savefig(f'{name}_clustering_dist.png')


def shortest_path_dist(G, name):
    nx.shortest_path_length(G)
    shpt_sequence = ([d for n, d in nx.shortest_path_length(G)])
    shptCount = collections.Counter()
    for x in range(len(shpt_sequence)):
        shptCount = shptCount + collections.Counter(shpt_sequence[x].values())
    deg, cnt = zip(*shptCount.items())
    fig, ax = plt.subplots(figsize=(20, 10))
    plt.bar(deg, cnt, width=0.80, color='b')
    plt.title("Shortest path distribution")
    plt.ylabel("Count")
    plt.xlabel("Shortest path length")
    ax.set_xticks([d + 0.4 for d in deg])
    ax.set_xticklabels(deg)
    plt.savefig(f'{name}_shortest_paths_dist.png')
